#!/usr/bin/python
import time
import vobject
import os

def read_vcards():
        pb_entries=0
        ich_entries=0
        och_entries=0
        mch_entries=0        
        # now read them in
        try:
            file=open("/tmp/CCH.vcf", "r")
        except Exception as e:
            log("PBAP: Exception opening vard info.")
            log(str(e))                    
            exit
            
        vcf=file.read()
        file.close()
        vcards=vobject.readComponents(vcf)
            
        for vcard in vcards:
            for data in vcard.getChildren():
                if data.name=="FN":
                    fn=data.value
                if data.name=="TEL":
                    number=data.value
                    
                if data.name=="X-IRMC-CALL-DATETIME":
                    datetime=data.value
                    for key in data.params.keys():
                        call_type=data.params[key][0]

            if fn=="":
                fn=number
            print call_type + " -- (" + fn + "): " + number + " @ " + datetime
            if call_type=="DIALED":
                och_entries+=1
            else:
                if call_type=="RECEIVED":
                    ich_entries+=1
                else:
                    if call_type=="MISSED":
                        mch_entries+=1


read_vcards()