import xbmcaddon, xbmcgui, xbmc
from threading import Thread
import time
import vobject
import dbus
import random
import dbus.service
import datetime
import os
import re
#import datetime
from datetime import date

from datetime import datetime
import _strptime
from dateutil.parser import parse
from dateutil.tz import tzlocal

#import anydbm

__addon__     = xbmcaddon.Addon()
__addonpath__ = __addon__.getAddonInfo('path').decode("utf-8")
__hf_addon__       = xbmcaddon.Addon(id='script.service.handsfree')
__hf_addondir__    = xbmc.translatePath( __hf_addon__.getAddonInfo('profile') )

SOUNDS_PATH = __addonpath__ + "/resources/media/"

#global used to tell the worker thread the status of the window
__windowopen__   = True
# global used to determine if this is the "small window" or the "large interface"
__full_interface__ = False

#capture a couple of actions to close the window
ACTION_PREVIOUS_MENU = 10
ACTION_BACK = 92

#controls for mini GUI
TITLE_LABEL  = 1302
CALL_LABEL   = 1303
ANSWER_LBL   = 1304
IGNORE_LBL   = 1305
ANSWER_BTN   = 1306
IGNORE_BTN   = 1307
CALLER_IMG   = 1308
HANGUP_LBL   = 1310
DURAT_LBL    = 1311
NUMBER_LBL   = 1312

#controls for full GUI
BACK_BTN     = 1301

DIAL_POP     = 1320
DIAL_IMG     = 1321
DIAL_LST     = 1322
DIAL_YES     = 1323
DIAL_NO      = 1324

ONE_BTN      = 1351
TWO_BTN      = 1352
THREE_BTN    = 1353
FOUR_BTN     = 1354
FIVE_BTN     = 1355
SIX_BTN      = 1356
SEVEN_BTN    = 1357
EIGHT_BTN    = 1358
NINE_BTN     = 1359
STAR_BTN     = 1360
ZERO_BTN     = 1350
POUND_BTN    = 1361
CLEAR_BTN    = 1362
DIAL_BTN     = 1363
DELETE_BTN   = 1364

CONTACT_BTN  = 1374
RECV_BTN     = 1375
DIALED_BTN   = 1376
MISSED_BTN   = 1377

CONTACT_LBL  = 1378
RECV_LBL     = 1379
DIALED_LBL   = 1380
MISSED_LBL   = 1381

CONTACT_LST  = 1382
RECV_LST     = 1383
DIALED_LST   = 1384
MISSED_LST   = 1385

DIALPAD      = 1398
CONTACTAREA  = 1399

# Logging facility.  For some reason this randomly throws errors in the CallRemoved signal
def log(logline):
    try:
        if __full_interface__==True:
            print "HFXML: " + logline
        else:
            print "HFMINI: " + logline
            
    except:
        pass

current_milli_time = lambda: int(round(time.time() * 1000))

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")    
    
    return value

def find_caller(call_id):
    
    # set in the service. If this is set, then the download is still happening, and we don't want to try and read anything
    # or it will throw an exception
    
    if get_kodi_prop("PBAP_download")=="1":
        return ""
    
    try:
        file=open(__hf_addondir__  + "PB.vcf", "r")
        vcf=file.read()
        file.close()
        vcards=vobject.readComponents(vcf)        
    except Exception as e:
        log("find_caller: Exception opening vard info.")
        log(str(e))                    
        return ""

    
    for vcard in vcards:
            tel_list=[]
            tel_type_list=[]
            match=0                 
            for data in vcard.getChildren():
                if data.name == "FN":
                    fn=data.value
                if data.name == "PHOTO":
                    photo=data.value
                        
                if data.params:
                    if data.name == "TEL":
                        if len(data.value)>5:
                            tel_list.append(data.value)
                            if call_id in data.value:
                                match=1
                            for key in data.params.keys():
                                for pbtype in data.params[key]:
                                    tel_type_list.append(pbtype)
            
            if match==1:
                log("Found match: " + fn)
                return fn
                break
            
            if fn!="" and len(tel_list)>0:
            #    log (fn + ": ")
                i=len(tel_list)
                j=0
                while j<i:
                    
             #       log("tel: " + str(tel_type_list[j]) + ": " +  str(tel_list[j]))
                    j+=1

def get_contact_numbers(contact_name, dial_list):
    
    # set in the service. If this is set, then the download is still happening, and we don't want to try and read anything
    # or it will throw an exception
    dial_list.reset()
    
    if get_kodi_prop("PBAP_download")=="1":
        return ""
    
    try:
        file=open(__hf_addondir__  + "PB.vcf", "r")
        vcf=file.read()
        file.close()
        vcards=vobject.readComponents(vcf)        
    except Exception as e:
        log("find_caller: Exception opening vard info.")
        log(str(e))                    
        return ""
    
    for vcard in vcards:
            tel_list=[]
            tel_type_list=[]
            match=0                 
            for data in vcard.getChildren():
                if data.name == "FN":
                    fn=data.value
                    if (fn==contact_name):
                        match=1
                if data.name == "PHOTO":
                    photo=data.value
                        
                if data.params:
                    if data.name == "TEL":
                        if len(data.value)>5:
                            tel_list.append(data.value)
                            for key in data.params.keys():
                                for pbtype in data.params[key]:
                                    tel_type_list.append(pbtype)
            
            if match==1:
                log (fn + ": ")
                i=len(tel_list)
                j=0
                while j<i:
                    dial_list.addItem(str(tel_list[j]))
                    num_type=str(tel_type_list[j]).lower().capitalize()
                    dial_list.getListItem(dial_list.size()-1).setLabel2("(" + num_type +")")
                    log("tel: " + str(tel_type_list[j]) + ": " +  str(tel_list[j]))
                    j+=1
                    


def updateWindow():
    global __windowopen__
    global __full_interface__
    
    # I put this here because sometimes the thread starts before the onInit function
    # is complete. When that happens, it throws an exception.
    # We can assume that once this is not None, it's done enough
    while handsfree.title_label==None:
        time.sleep(.25)

        
    #this is the worker thread that updates the window information every w seconds
    #this strange looping exists because I didn't want to sleep the thread for very long
    #as time.sleep() keeps user input from being acted upon
    
    while __windowopen__ and (not xbmc.abortRequested):

        bt_conn_name=get_kodi_prop("connected_device")
        if bt_conn_name=="":
            time.sleep(1)
            continue
        
        if __full_interface__==False:
            # this section will handle the interface for incoming/outgoing calls
            
            # this is just to establish a call time. It will get overwritten when the call is active below
            # with the actual starttime properties from the call, but we need something in the variable now
            call_start = datetime.now(tzlocal())
        
            try:
                vp=handsfree.voice_call.GetProperties()
                state=vp["State"]
                set_kodi_prop("call_state", state)
                if state=="active":
                    handsfree.title_label.setLabel("Active call with")
                    # I hate doing this here, because it's going to be executed once per second while on a call
                    # But when an incoming call is presented, and answered on a physical phone there doesn't seem to be a
                    # property change signal thrown, so there is no way to know the call has been answered.
                    xbmc.stopSFX()
                    call_start_str=vp["StartTime"]
                    call_start=parse(call_start_str)
            except:
            # the voice call object has failed, we'll silently do nothing though
            	pass

            now = datetime.now(tzlocal())
            diff=now-call_start
            seconds=diff.seconds
            minutes, seconds = divmod(seconds, 60)
            hours, minutes= divmod(minutes, 60)
            hours=format(hours, "02d")
            minutes=format(minutes, "02d")
            seconds=format(seconds, "02d")
            handsfree.durat_label.setLabel(hours + ":" + minutes + ":" + seconds)
            
            if get_kodi_prop("hfp_close")!="":
                log("ENDING hfpclose.")        
                set_kodi_prop("call_id", "")
                set_kodi_prop("caller_name", "")
                set_kodi_prop("call_state", "")
                set_kodi_prop("call_direction", "")            
                set_kodi_prop("call_path", "");
                set_kodi_prop("handsfree_active", "")
                __windowopen__ = False
                xbmc.executebuiltin('XBMC.Action(PreviousMenu)')
 
        else:
            # handling the full interface
            pass
        # give us a break
        time.sleep(1)
                                                                                                                                                
class handsfree(xbmcgui.WindowXMLDialog):
    
    title_label=None
    call_label=None
    answer_label=None
    ignore_label=None
    durat_label=None    
    answer_btn=None
    ignore_btn=None
    caller_img=None
    voice_call=None

    caller_display=None
    call_direction=None
    call_state=None
    back_btn=None
    
    title=""

    one_btn=None
    two_btn=None
    three_btn=None
    four_btn=None
    five_btn=None
    six_btn=None
    seven_btn=None
    eight_btn=None
    nine_btn=None
    star_btn=None
    zero_btn=None
    pound_btn=None
    clear_btn=None
    dial_btn=None
    delete_btn=None

    contact_scrl=None
    dialed_scrl=None
    recv_scrl=None
    missed_scrl=None
    
    contact_btn=None
    dialed_btn=None
    recv_btn=None
    missed_btn=None

    contact_label=None
    dialed_label=None
    recv_label=None
    missed_label=None

    contact_list=None
    dialed_list=None
    recv_list=None
    missed_list=None
    
    number_to_dial=""
    clean_number=""
    number_lbl=None

    dialpad=None
    contactarea=None    

    dial_pop=None
    dial_image=None
    dial_list=None
    dial_yes=None
    dial_no=None
	
    bt_conn_name=None

    # these are arrays that back up the controllists
    # basically so I can sort the contact list alphabetically
    contact_array_list = []
    
    def __init__(self,strXMLname, strFallbackPath, strDefaultName, forceFallback):
        log("Initializing...")

    def onInit(self):

        if __full_interface__==True:
            bt_conn_name=get_kodi_prop("connected_device")
            log("BT connected device: " + bt_conn_name)
            if bt_conn_name=="":
                xbmcgui.Dialog().ok("Device missing","No bluetooth devices are connected.")
                log("ENDING. no device.")
                set_kodi_prop("call_id", "")
                set_kodi_prop("caller_name", "")
                set_kodi_prop("call_state", "")
                set_kodi_prop("call_direction", "")            
                set_kodi_prop("call_path", "");
                set_kodi_prop("handsfree_active", "")
                __windowopen__ = False
                self.close()
                return
            else:
                log("Main area")
   	        title="Bluetooth Dialing"
                handsfree.back_btn=self.getControl(BACK_BTN)
                # dial pad
                handsfree.one_btn=self.getControl(ONE_BTN)
                handsfree.two_btn=self.getControl(TWO_BTN)
                handsfree.three_btn=self.getControl(THREE_BTN)
                handsfree.four_btn=self.getControl(FOUR_BTN)
                handsfree.five_btn=self.getControl(FIVE_BTN)
                handsfree.six_btn=self.getControl(SIX_BTN)
                handsfree.seven_btn=self.getControl(SEVEN_BTN)
                handsfree.eight_btn=self.getControl(EIGHT_BTN)
                handsfree.nine_btn=self.getControl(NINE_BTN)
                handsfree.star_btn=self.getControl(STAR_BTN)
                handsfree.zero_btn=self.getControl(ZERO_BTN)
                handsfree.pound_btn=self.getControl(POUND_BTN)
                handsfree.clear_btn=self.getControl(CLEAR_BTN)
                handsfree.dial_btn=self.getControl(DIAL_BTN)
                handsfree.delete_btn=self.getControl(DELETE_BTN)
                # dialing number label
                handsfree.number_lbl=self.getControl(NUMBER_LBL)

            # buttons/labels,lists for PBAP data
                handsfree.contact_btn=self.getControl(CONTACT_BTN)
                handsfree.dialed_btn=self.getControl(DIALED_BTN)
                handsfree.recv_btn=self.getControl(RECV_BTN)
                handsfree.missed_btn=self.getControl(MISSED_BTN)
                handsfree.contact_label=self.getControl(CONTACT_LBL)
                handsfree.dialed_label=self.getControl(DIALED_LBL)
                handsfree.recv_label=self.getControl(RECV_LBL)
                handsfree.missed_label=self.getControl(MISSED_LBL)
                handsfree.contact_list=self.getControl(CONTACT_LST)
                handsfree.recv_list=self.getControl(RECV_LST)            
                handsfree.dialed_list=self.getControl(DIALED_LST)
                handsfree.missed_list=self.getControl(MISSED_LST)
            
                handsfree.dialpad=self.getControl(DIALPAD)
                handsfree.contactarea=self.getControl(CONTACTAREA)

                handsfree.dial_pop=self.getControl(DIAL_POP)
                handsfree.dial_image=self.getControl(DIAL_IMG)
                handsfree.dial_list=self.getControl(DIAL_LST)
                handsfree.dial_yes=self.getControl(DIAL_YES)
                handsfree.dial_no=self.getControl(DIAL_NO)  
            
                #set the dial pop to be disabled

                handsfree.dial_pop.setEnabled(False)
                handsfree.dial_pop.setVisible(False)            
            
                # want these all disabled and invis to start, except contacts
                handsfree.recv_list.setVisible(False)
                handsfree.recv_list.setEnabled(False)
                handsfree.dialed_list.setVisible(False)
                handsfree.dialed_list.setEnabled(False)
                handsfree.missed_list.setVisible(False)
                handsfree.missed_list.setEnabled(False)            

                # set the number blank
                handsfree.number_lbl.setLabel("")
                handsfree.number_to_dial=get_kodi_prop("last_dialed")
                handsfree.number_lbl.setLabel(handsfree.number_to_dial)                
                # load the lists - only for the full interface though
                self.populate_contacts()
                self.populate_call_lists()
            
        if __full_interface__==False:
            handsfree.answer_btn=self.getControl(ANSWER_BTN)
            handsfree.ignore_btn=self.getControl(IGNORE_BTN)            
            handsfree.call_label=self.getControl(CALL_LABEL)
            handsfree.answer_label=self.getControl(ANSWER_LBL)
            handsfree.ignore_label=self.getControl(IGNORE_LBL)
            handsfree.hangup_label=self.getControl(HANGUP_LBL)
            handsfree.durat_label=self.getControl(DURAT_LBL)
        
            handsfree.durat_label.setVisible(False)
        
            handsfree.caller_img=self.getControl(CALLER_IMG)
            handsfree.hangup_label.setVisible(False)
            
            caller_name=get_kodi_prop("caller_name")
            
            caller_image_str=self.load_photo(caller_name)
            handsfree.caller_img.setImage(caller_image_str)
            handsfree.caller_display=get_kodi_prop("caller_display")
            handsfree.call_state=get_kodi_prop("call_state")
            handsfree.call_direction=get_kodi_prop("call_direction")
            handsfree.call_path=get_kodi_prop("call_path")

            if handsfree.call_direction=="incoming":
                title="Incoming call from"
            else:
                title="Outgoing call to"
                handsfree.answer_label.setVisible(False)
                handsfree.durat_label.setVisible(True)
                handsfree.answer_btn.setVisible(False)
                handsfree.ignore_btn.setPosition(896,450)
                handsfree.ignore_label.setPosition(896,560)
                handsfree.ignore_label.setLabel("Hang up")

            handsfree.call_label.setLabel(handsfree.caller_display)
            
        # This is loaded last, because we use it in the main looping thread
        # to make sure this function has completed initialization
        handsfree.title_label=self.getControl(TITLE_LABEL)
        handsfree.title_label.setLabel(title)
        
        # if we're not connected to a BT device, don't load this
        if get_kodi_prop("connected_device")=="1":
            xbmcgui.Dialog().ok("Not connected","Not connected to a bluetooth device.")
            log("ENDING. no device")
            set_kodi_prop("call_id", "")
            set_kodi_prop("caller_name", "")
            set_kodi_prop("call_state", "")
            set_kodi_prop("call_direction", "")            
            set_kodi_prop("call_path", "");
            set_kodi_prop("handsfree_active", "")
            __windowopen__ = False            
            self.close()
            
        # do dbus connectivity
        log("Connecting to DBUS...")
        self.bus = dbus.SystemBus()
        log("Connected to DBUS...")        
        try:
            self.manager = dbus.Interface(self.bus.get_object('org.ofono',
            '/'), dbus_interface = 'org.ofono.Manager')
            log("Connected with Ofono via DBUS")
        except:
            xbmcgui.Dialog().ok("Ofono missing","Could not get Ofono via DBUS.")
            log("ENDING. no ofono")
            set_kodi_prop("call_id", "")
            set_kodi_prop("caller_name", "")
            set_kodi_prop("call_state", "")
            set_kodi_prop("call_direction", "")            
            set_kodi_prop("call_path", "");
            set_kodi_prop("handsfree_active", "")
            __windowopen__ = False            
            self.close()
            return
        
        self.bus.add_signal_receiver(self.card_prop_change,
            bus_name="org.ofono",
            dbus_interface="org.ofono.HandsfreeAudioCard",
            signal_name="PropertyChanged")

        self.bus.add_signal_receiver(self.VC_Prop_Change,
            bus_name="org.ofono",
            dbus_interface="org.ofono.VoiceCall",
            signal_name="PropertyChanged")
            
        self.bus.add_signal_receiver(self.CallAdded,
        	bus_name="org.ofono",
            dbus_interface="org.ofono.VoiceCallManager",
            signal_name="CallAdded")

        self.bus.add_signal_receiver(self.CallRemoved,
        	bus_name="org.ofono",
            dbus_interface="org.ofono.VoiceCallManager",
            signal_name="CallRemoved")

        self.bus.add_signal_receiver(self.CM_Prop_Change,
        	bus_name="org.ofono",
            dbus_interface="org.ofono.VoiceCallManager",
            signal_name="PropertyChanged")
        
        
        # connect to the call, but only if this is not the full interface
        if __full_interface__==False:
            handsfree.voice_call = dbus.Interface(self.bus.get_object('org.ofono',
            handsfree.call_path), dbus_interface = 'org.ofono.VoiceCall')
            log("Connected to voice call.")
            # it's possible that the emulated ring is still happening. this should stop it.

        else:
            
            modems = self.manager.GetModems()            
            
            for path, properties in modems:

                if "org.ofono.ConnectionManager" not in properties["Interfaces"]:
                    handsfree.conman=dbus.Interface(self.bus.get_object('org.ofono', path),  
                                                        'org.ofono.ConnectionManager')

                if "org.ofono.NetworkRegistration" in properties["Interfaces"]:
                    handsfree.netman=dbus.Interface(self.bus.get_object('org.ofono', path),  
                                                        'org.ofono.NetworkRegistration')

                if "org.ofono.VoiceCallManager" in properties["Interfaces"]:
                    handsfree.voice_man=dbus.Interface(self.bus.get_object('org.ofono', path),  
                                                        'org.ofono.VoiceCallManager')

                if "org.ofono.CallVolume" in properties["Interfaces"]:
                    handsfree.call_vol=dbus.Interface(self.bus.get_object('org.ofono', path),
                                                        'org.ofono.CallVolume')

                if "org.ofono.Handsfree" in properties["Interfaces"]:
                    handsfree.hf=dbus.Interface(self.bus.get_object('org.ofono', path),
                                                        'org.ofono.Handsfree')
                        
                                                        
    def VC_Prop_Change(self, path, properties):
        log("VC Prop change: " +  properties)        
        if path=="State":
            log("VC Prop change: " +  properties)
            handsfree.call_state=get_kodi_prop("call_state")
            if handsfree.call_state!="active" and properties=="active":
                log("Changed to active call!")
                if get_kodi_prop("call_direction")=="dialing":
                    xbmc.stopSFX()                
                
                handsfree.call_state=properties
                # only animate this when it's an incoming call
                if handsfree.call_direction=="incoming":
                    handsfree.answer_btn.setAnimations([('conditional','effect=fade start=100 end=0 time=500 condition=true',)])
                    handsfree.answer_label.setAnimations([('conditional','effect=fade start=100 end=0 time=500 condition=true',)])
                    handsfree.ignore_btn.setAnimations([('conditional','effect=slide end=-128 time=500 condition=true',)])
                    handsfree.ignore_label.setAnimations([('conditional','effect=slide end=-64 time=500 condition=true',),
                    ('conditional','effect=fade start=100 end=0 time=500 condition=true',)])
                    handsfree.hangup_label.setVisible(True)
                    handsfree.hangup_label.setAnimations([('conditional','effect=slide end=-72 time=500 condition=true',),
                    ('conditional','effect=fade start=0 end=100 time=500 condition=true',)])
                    handsfree.hangup_label.setAnimations([('conditional','effect=slide end=-72 time=500 condition=true',),
                    ('conditional','effect=fade start=0 end=100 time=500 condition=true',)])
                    
                    handsfree.durat_label.setVisible(True)
                    handsfree.durat_label.setAnimations([('conditional','effect=fade start=0 end=100 time=1000 condition=true',)])
                
                    

    def CM_Prop_Change(self, path, properties):
        log ("CM: prop change")
        #print path
        #print properties

        
    def CallAdded(self, path, properties):
        log ("Call added (" + properties["State"] + "): " + properties["LineIdentification"])
        # TODO Deal with this on a secondary call?

        
    def CallRemoved(self, path):
        log("Call Removed")
        xbmc.stopSFX()

    def VC_prop_change(self, name, value):
        log("Call property change: '" + name + "': '" + value + "'")
        
        
    def card_prop_change(self, name, value):
        log("Card property change: '" + name + "': '" + value + "'")
        
    def onAction(self, action):
        if action == ACTION_PREVIOUS_MENU:
            log("ENDING. prev menu")
            if __full_interface__==False:
                set_kodi_prop("call_id", "")
                set_kodi_prop("caller_name", "")
                set_kodi_prop("call_state", "")
                set_kodi_prop("call_direction", "")            
                set_kodi_prop("call_path", "");
                set_kodi_prop("handsfree_active", "")
            __windowopen__ = False
            self.close()

        if (action.getId()>=58 and action.getId()<=67):
            number=int(action.getId())-58
            handsfree.number_to_dial=handsfree.number_to_dial+str(number)
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-"+str(number)+".wav")
            
    def onKey(self, controlID):
        log("Key pressed: " + str(controlID))
        
    def onClick(self, controlID):
        log("On click: " + str(controlID))
        number_trunc=0
        
        if controlID == BACK_BTN:
            log("ENDING. back button")
            set_kodi_prop("call_id", "")
            set_kodi_prop("caller_name", "")
            set_kodi_prop("call_state", "")
            set_kodi_prop("call_direction", "")            
            set_kodi_prop("call_path", "");
            set_kodi_prop("handsfree_active", "")
            __windowopen__ = False            
            self.close()
        
        if controlID == ANSWER_BTN:
            handsfree.voice_call.Answer()
            # Stop the incoming ringing sound.
            xbmc.stopSFX()
            
        if controlID == IGNORE_BTN:
            log("ENDING. ignore btn")
            handsfree.voice_call.Hangup()
            set_kodi_prop("call_id", "")
            set_kodi_prop("caller_name", "")
            set_kodi_prop("call_state", "")
            set_kodi_prop("call_direction", "")            
            set_kodi_prop("call_path", "");
            set_kodi_prop("handsfree_active", "")
            xbmc.stopSFX()            
            __windowopen__ = False            
            self.close()
        
        if controlID == ONE_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"1"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-1.wav")

        if controlID == TWO_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"2"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-2.wav")            

        if controlID == THREE_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"3"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-3.wav")            

        if controlID == FOUR_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"4"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-4.wav")

        if controlID == FIVE_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"5"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-5.wav")            

        if controlID == SIX_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"6"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-6.wav")
            
        if controlID == SEVEN_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"7"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-7.wav")            

        if controlID == EIGHT_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"8"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-8.wav")
            
        if controlID == NINE_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"9"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-9.wav")            
            
        if controlID == ZERO_BTN:
            xbmc.playSFX(SOUNDS_PATH + "dtmf-0.wav")            
            if handsfree.number_to_dial=="":
                handsfree.number_to_dial=handsfree.number_to_dial+"+"
            else:
                handsfree.number_to_dial=handsfree.number_to_dial+"0"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)

        if controlID == STAR_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"*"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-star.wav")            

        if controlID == POUND_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial+"#"
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            xbmc.playSFX(SOUNDS_PATH + "dtmf-hash.wav")

        if controlID == CLEAR_BTN:
            handsfree.number_to_dial=""
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)

        if controlID == DELETE_BTN:
            handsfree.number_to_dial=handsfree.number_to_dial[:-1]
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)

        if len(handsfree.number_to_dial)>13:
            number_trunc=len(handsfree.number_to_dial)-13
            handsfree.number_lbl.setLabel(handsfree.number_to_dial[number_trunc:])

        if controlID == CONTACT_BTN:
            handsfree.contact_label.setLabel("[COLOR white]Contacts[/COLOR]")
            handsfree.dialed_label.setLabel("[COLOR grey]Dialed Calls[/COLOR]")
            handsfree.recv_label.setLabel("[COLOR grey]Received Calls[/COLOR]")
            handsfree.missed_label.setLabel("[COLOR grey]Missed Calls[/COLOR]")
            handsfree.contact_list.setVisible(True)
            handsfree.contact_list.setEnabled(True)
            handsfree.recv_list.setVisible(False)
            handsfree.recv_list.setEnabled(False)
            handsfree.dialed_list.setVisible(False)
            handsfree.dialed_list.setEnabled(False)
            handsfree.missed_list.setVisible(False)
            handsfree.missed_list.setEnabled(False)            

        if controlID == DIALED_BTN:
            handsfree.contact_label.setLabel("[COLOR grey]Contacts[/COLOR]")
            handsfree.dialed_label.setLabel("[COLOR white]Dialed Calls[/COLOR]")
            handsfree.recv_label.setLabel("[COLOR grey]Received Calls[/COLOR]")
            handsfree.missed_label.setLabel("[COLOR grey]Missed Calls[/COLOR]")
            handsfree.contact_list.setVisible(False)
            handsfree.contact_list.setEnabled(False)
            handsfree.recv_list.setVisible(False)
            handsfree.recv_list.setEnabled(False)
            handsfree.dialed_list.setVisible(True)
            handsfree.dialed_list.setEnabled(True)
            handsfree.missed_list.setVisible(False)
            handsfree.missed_list.setEnabled(False)            
            
        if controlID == RECV_BTN:
            handsfree.contact_label.setLabel("[COLOR grey]Contacts[/COLOR]")
            handsfree.dialed_label.setLabel("[COLOR grey]Dialed Calls[/COLOR]")
            handsfree.recv_label.setLabel("[COLOR white]Received Calls[/COLOR]")
            handsfree.missed_label.setLabel("[COLOR grey]Missed Calls[/COLOR]")
            handsfree.contact_list.setVisible(False)
            handsfree.contact_list.setEnabled(False)
            handsfree.recv_list.setVisible(True)
            handsfree.recv_list.setEnabled(True)
            handsfree.dialed_list.setVisible(False)
            handsfree.dialed_list.setEnabled(False)
            handsfree.missed_list.setVisible(False)
            handsfree.missed_list.setEnabled(False)            
            
        if controlID == MISSED_BTN:
            this_list=None
            handsfree.contact_label.setLabel("[COLOR grey]Contacts[/COLOR]")
            handsfree.dialed_label.setLabel("[COLOR grey]Dialed Calls[/COLOR]")
            handsfree.recv_label.setLabel("[COLOR grey]Received Calls[/COLOR]")
            handsfree.missed_label.setLabel("[COLOR white]Missed Calls[/COLOR]")            
            handsfree.contact_list.setVisible(False)
            handsfree.contact_list.setEnabled(False)
            handsfree.recv_list.setVisible(False)
            handsfree.recv_list.setEnabled(False)
            handsfree.dialed_list.setVisible(False)
            handsfree.dialed_list.setEnabled(False)
            handsfree.missed_list.setVisible(True)
            handsfree.missed_list.setEnabled(True)            
            
        if controlID == CONTACT_LST or controlID == DIALED_LST or controlID == RECV_LST or controlID == MISSED_LST:

            if controlID == CONTACT_LST:
                this_list=handsfree.contact_list
            if controlID == RECV_LST:
                this_list=handsfree.recv_list
            if controlID == DIALED_LST:
                this_list=handsfree.dialed_list
            if controlID == MISSED_LST:
                this_list=handsfree.missed_list                

            position=this_list.getSelectedPosition()
            item = this_list.getSelectedItem()
            log("Selected position: " + str(position))
            log("item: " + item.getLabel())
            # reset the dial list
            handsfree.dial_list.reset()            
            # This is weird.  When I animate these, they shift left.
            # but if I assign the coordinates to that of the XML file, they are fine?
            
            handsfree.dialpad.setPosition(50,0)
            handsfree.contactarea.setPosition(100,0)            
            handsfree.dialpad.setAnimations([('conditional','effect=fade start=100 end=25 time=100 condition=true',)])
            handsfree.contactarea.setAnimations([('conditional','effect=fade start=100 end=25 time=100 condition=true',)])
            handsfree.dialpad.setEnabled(False)
            handsfree.contactarea.setEnabled(False)
            handsfree.dial_pop.setVisible(True)
            handsfree.dial_pop.setAnimations([('conditional','effect=fade start=0 end=100 time=100 condition=true',)])
            handsfree.dial_pop.setEnabled(True)
            handsfree.dial_list.setEnabled(True)
            
            # special case for contacts. We need to look up the phone numbers
            if controlID == CONTACT_LST:
                caller_image_str=self.load_photo(item.getLabel())
                handsfree.dial_image.setImage(caller_image_str)
                get_contact_numbers(item.getLabel(), handsfree.dial_list)
            else:
                caller_image_str=self.load_photo(item.getLabel())
                handsfree.dial_image.setImage(caller_image_str)
                handsfree.dial_list.addItem(item.getLabel())
                handsfree.number_to_dial=item.getLabel2()
                log("number: " + handsfree.number_to_dial)
                handsfree.number_lbl.setLabel(handsfree.number_to_dial)

            
            
        if controlID == DIAL_NO:
            # hide the window, and bring the dial interface back to the foreground
            handsfree.dialpad.setAnimations([('conditional','effect=fade start=25 end=100 time=100 condition=true',)])
            handsfree.contactarea.setAnimations([('conditional','effect=fade start=25 end=100 time=100 condition=true',)])
            handsfree.dialpad.setEnabled(True)
            handsfree.contactarea.setEnabled(True)
            handsfree.dial_pop.setAnimations([('conditional','effect=fade start=100 end=0 time=100 condition=true',)])
            handsfree.dial_pop.setEnabled(False)
            handsfree.dial_pop.setVisible(False)

        if controlID == DIAL_LST:
            pass
        
        if controlID == DIAL_YES:
            position=handsfree.dial_list.getSelectedPosition()
            item = handsfree.dial_list.getSelectedItem()
            handsfree.number_to_dial=item.getLabel()
            handsfree.number_lbl.setLabel(handsfree.number_to_dial)
            
            # hide the window, and bring the dial interface back to the foreground
            handsfree.dialpad.setAnimations([('conditional','effect=fade start=25 end=100 time=100 condition=true',)])
            handsfree.contactarea.setAnimations([('conditional','effect=fade start=25 end=100 time=100 condition=true',)])
            handsfree.dialpad.setEnabled(True)
            handsfree.contactarea.setEnabled(True)
            handsfree.dial_pop.setAnimations([('conditional','effect=fade start=100 end=0 time=100 condition=true',)])
            handsfree.dial_pop.setEnabled(False)
            handsfree.dial_pop.setVisible(False)
            # clean up the number so it dials in the API correctly.
            handsfree.clean_number=handsfree.number_to_dial
            pattern = re.compile(r'[() -]')
            handsfree.clean_number = re.sub(pattern, '',  handsfree.clean_number)
            log("number: " + handsfree.number_to_dial)
            log("clean : " + handsfree.clean_number)
            handsfree.voice_man.Dial(handsfree.clean_number, "")
            xbmc.playSFX(SOUNDS_PATH + "outbound_ring.wav")
            set_kodi_prop("last_dialed", handsfree.number_to_dial)

        if controlID == DIAL_BTN:
            if len(handsfree.number_to_dial)>1:
                handsfree.voice_man.Dial(handsfree.number_to_dial, "")
                xbmc.playSFX(SOUNDS_PATH + "outbound_ring.wav")
                set_kodi_prop("last_dialed", handsfree.number_to_dial)
            
    def onFocus(self, controlID):
        pass
    
    def onControl(self, controlID):
        pass


    def load_photo(self, name):
        image_str=__hf_addondir__ + name + ".jpg"
        # If there isn't an image for this caller load a silhouette
        if os.path.isfile(image_str)==False:
            silhouette=random.randint(1, 4)
            name="silhouette" + str(silhouette)
            image_str=name + ".png"
        return image_str
        
    def populate_contacts(self):
    
        # set in the service. If this is set, then the download is still happening, and we don't want to try and read anything
        # or it will throw an exception
    
        if get_kodi_prop("PBAP_download")=="1":
            return ""
        
        try:
            file=open(__hf_addondir__  + "PB.vcf", "r")
            vcf=file.read()
            file.close()
            vcards=vobject.readComponents(vcf)        
        except Exception as e:
            log("populate_contacts: Exception opening vard info.")
            log(str(e))                    
            
        for vcard in vcards:
            tel_list=[]
            tel_type_list=[]
            for data in vcard.getChildren():
                if data.name == "FN":
                    fn=data.value
                if data.name == "PHOTO":
                    photo=data.value
                        
                if data.params:
                    if data.name == "TEL":
                        if len(data.value)>5:
                            tel_list.append(data.value)
            
            if fn!="" and len(tel_list)>0:
                handsfree.contact_array_list.append(fn)
#               log("Adding " + fn + " to array")
    

        for item in sorted(handsfree.contact_array_list):
            handsfree.contact_list.addItem(item)
            image_str=self.load_photo(item)
            handsfree.contact_list.getListItem(handsfree.contact_list.size()-1).setIconImage(image_str)
            
    def populate_call_lists(self):
    
        # set in the service. If this is set, then the download is still happening, and we don't want to try and read anything
        # or it will throw an exception
    
        if get_kodi_prop("PBAP_download")=="1":
            return ""
        
        try:
            file=open(__hf_addondir__  + "CCH.vcf", "r")
            vcf=file.read()
            file.close()
            vcards=vobject.readComponents(vcf)        
        except Exception as e:
            log("populate_contacts: Exception opening vard info.")
            log(str(e))
        start_m=current_milli_time()
        log("Start: " + str(start_m))
        for vcard in vcards:
            call_datetime=None
            for data in vcard.getChildren():
                if data.name=="FN":
                    fn=data.value
                if data.name=="TEL":
                    number=data.value
                    
                if data.name=="X-IRMC-CALL-DATETIME":
                    call_datetime=data.value
                    for key in data.params.keys():
                        call_type=data.params[key][0]
            if fn=="":
                fn=number

            if call_datetime:
#                log(call_type + " -- (" + fn + "): " + number + " @ " + call_datetime)
                #dt=date.timefromtimestamp(time.mktime(time.strptime(call_datetime, "%Y%m%dT%H%M%S")))
                dt=datetime.fromtimestamp(time.mktime(time.strptime(call_datetime, "%Y%m%dT%H%M%S")))
                
                time_str=format(dt.month, "02d") + "/" + format(dt.month, "02d") + "/" + format(dt.year, "02d") + "[CR]"
                if dt.hour >=12:
                    ampm="PM"
                    hour=dt.hour-12
                else:
                    ampm="AM"
                    hour=dt.hour
                time_str=time_str+" " + format(dt.hour,"02d")+":"+format(dt.minute,"02d")+ ampm

            if call_type=="RECEIVED":
                handsfree.recv_list.addItem(fn)
                image_str=self.load_photo(fn)
                size=handsfree.recv_list.size()-1
                handsfree.recv_list.getListItem(size).setIconImage(image_str)
                # try and detect if this is a name we know, if so show the number.
                # because otherwise, it is the number

                if (not fn.isalnum()) and (")" not in fn and "#" not in fn and "*" not in fn):
                    handsfree.recv_list.getListItem(size).setLabel2(number)

                handsfree.recv_list.getListItem(size).setInfo("music", {"Artist": time_str})

            if call_type=="DIALED":
                handsfree.dialed_list.addItem(fn)
                image_str=self.load_photo(fn)
                handsfree.dialed_list.getListItem(handsfree.dialed_list.size()-1).setIconImage(image_str)
                # try and detect if this is a name we know, if so show the number.
                # because otherwise, it is the number
                if (not fn.isalnum()) and (")" not in fn and "#" not in fn and "*" not in fn):
                    handsfree.dialed_list.getListItem(handsfree.dialed_list.size()-1).setLabel2(number)                    
                handsfree.dialed_list.getListItem(handsfree.dialed_list.size()-1).setInfo("music", {"Artist": time_str})
                
            if call_type=="MISSED":
                handsfree.missed_list.addItem(fn)
                image_str=self.load_photo(fn)
                handsfree.missed_list.getListItem(handsfree.missed_list.size()-1).setIconImage(image_str)
                # try and detect if this is a name we know, if so show the number.
                # because otherwise, it is the number
                if (not fn.isalnum()) and (")" not in fn and "#" not in fn and "*" not in fn):
                    handsfree.missed_list.getListItem(handsfree.missed_list.size()-1).setLabel2(number)                    
                handsfree.missed_list.getListItem(handsfree.missed_list.size()-1).setInfo("music", {"Artist": time_str})
        end_m=current_milli_time()
        log("End: " + str(end_m))
        log("Diff: " + str(end_m-start_m))
        
if  __name__ == '__main__':    
    
    t1 = Thread( target=updateWindow)
    t1.setDaemon( True )
    t1.start()

    # so, if this is set, it was set by the call handler in the HF service.
    # this will then run the hfdialog, which is just a pop up interface.

    # if it not set, then we will run the full GUI interface

    if get_kodi_prop("call_id")=="":
        __full_interface__=True
        hf_full_dialog = handsfree("handsfreefull.xml", __addonpath__, 'default', '720p')
        hf_full_dialog.doModal()
        #del handsfree
        del hf_full_dialog

    else:
        __full_interface__=False        
        xbmc.playSFX(SOUNDS_PATH + "outbound_ring.wav")
        call_id=get_kodi_prop("call_id")        
        set_kodi_prop("handsfree_active", "1")
        set_kodi_prop("caller_name", "")
        set_kodi_prop("caller_display", call_id)

        # try and find a name that matches the number in the PB
        caller_name=find_caller(call_id)
    
        # if not, check if this US number is 11 digits and try again removing the leading 1
        if caller_name==None:
            if len(call_id)==11 and call_id[0]=='1':
                caller_name=find_caller(call_id[1:])

        if caller_name!=None:
            set_kodi_prop("caller_name", caller_name)        
            set_kodi_prop("caller_display", caller_name + " (" + call_id + ")")
        
        
        hfdialog = handsfree("handsfree.xml", __addonpath__, 'default', '720p')
        hfdialog.doModal()
        #del handsfree
        del hfdialog
        